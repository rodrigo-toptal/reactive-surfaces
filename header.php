<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Reactive_Surfaces
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'reactivesurfaces' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			if ( has_custom_logo() ) {

				// Display custom logo (white logo) on the home page, otherwise display black logo
				if ( is_front_page() ) {
					the_custom_logo();
				} else {
					$logo_url = get_stylesheet_directory_uri() . '/images/logo-new.png';
					?>
					<a href="<?php echo site_url(); ?>" class="custom-logo-link" rel="home" itemprop="url">
						<img width="367" height="113" src="<?php echo $logo_url; ?>" class="custom-logo" alt="Reactive Surfaces" itemprop="logo" srcset="<?php echo $logo_url; ?> 367w, <?php echo $logo_url; ?> 300w" sizes="(max-width: 367px) 100vw, 367px">
					</a>
					<?php
				}

			} else {
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif;
				$reactivesurfaces_description = get_bloginfo( 'description', 'display' );
				if ( $reactivesurfaces_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $reactivesurfaces_description; /* WPCS: xss ok. */ ?></p>
				<?php endif;
			} // End has_custom_logo check ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<span id="mobile-navigation-toggle"><i class="fa fa-bars"></i></span>
			<?php /*
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'reactivesurfaces' ); ?></button>
			*/ ?>

			<?php
			wp_nav_menu( array(
				'theme_location' => 'secondary',
				'menu_id'        => 'secondary-menu',
			) );
			?>

			<?php
			wp_nav_menu( array(
				'theme_location' => 'primary',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav><!-- #site-navigation -->

		<nav id="mobile-navigation" class="mobile-navigation mobile-overlay">
			<span id="mobile-navigation-close"><i class="fa fa-times"></i></span>
			<div class="overlay-content">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'primary',
					'menu_id'        => 'primary-menu-mobile',
				) );
				?>
			</div>
			<div class="social-media-navigation">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'social',
					'menu_id'        => 'social-media-menu-mobile',
				) );
				?>
			</div>
		</nav><!-- #mobile-navigation -->

	</header><!-- #masthead -->

	<?php if ( is_front_page() ) : ?>
		<div class="header-image-container">
			<figure class="header-image">
				<?php the_header_image_tag(); ?>
			</figure>

			<div class="header-text-container">
				<a href="/contact-us" class="anti-covid-message">‘Anti-Covid Surfaces’…We’re On it! <span style="padding-left: 1em;">Click here for more information.</span></a>
				<h1>What has your coating done for YOU lately?</h1>
				<h2>Reactive Surfaces is the industry leader in bringing dynamic functionality to our client’s paints and coatings and other materials.</h2>
				<p>We don’t MAKE paint…We make paint WORK!</p>
				<p>Put your coating to work for YOU today.</p>
			</div>

			<img class="header-navigation-arrow" alt="Scroll Down" src="<?php echo get_stylesheet_directory_uri() . '/images/navigation-arrow.png'; ?>">
		</div><!-- .header-image-container -->
	<?php endif; ?>

	<div id="content" class="site-content">
