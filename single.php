<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Reactive_Surfaces
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'single' );

			$args = array(
				'prev_text' => '<i class="fa fa-arrow-left"></i> ' . __( 'Previous Article: %title', 'reactivesurfaces' ),
				'next_text' => __( 'Next Article: %title', 'reactivesurfaces' ) . ' <i class="fa fa-arrow-right"></i> ',
			);
			the_post_navigation( $args );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
