<?php
/**
 * Template Name: Technology
 *
 * The template for displaying Technology Page and its custom modules
 *
 * @package Reactive_Surfaces
 */

get_header();
?>

	<div id="primary" class="content-area template-technology">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'technology' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
