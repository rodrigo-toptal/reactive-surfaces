<?php
/**
 * Reactive Surfaces functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Reactive_Surfaces
 */

if ( ! function_exists( 'reactivesurfaces_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function reactivesurfaces_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Reactive Surfaces, use a find and replace
		 * to change 'reactivesurfaces' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'reactivesurfaces', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in several locations.
		register_nav_menus( array(
			'primary'   => esc_html__( 'Primary Menu', 'reactivesurfaces' ),
			'secondary' => esc_html__( 'Secondary Menu', 'reactivesurfaces' ),
			'footer'    => esc_html__( 'Footer Menu', 'reactivesurfaces' ),
			'social'    => esc_html__( 'Social Media Links Menu', 'reactivesurfaces' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'reactivesurfaces_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 367,
			'width'       => 113,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'reactivesurfaces_setup' );

/**
 * Register custom fonts.
 */
function reactivesurfaces_fonts_url() {
	$fonts_url = '';
//https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,600,700
	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Montserrat, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$montserrat = _x( 'on', 'Montserrat font: on or off', 'reactivesurfaces' );

	if ( 'off' !== $montserrat ) {
		$font_families = array();

		$font_families[] = 'Montserrat:300,300i,400,400i,500,600,700';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function reactivesurfaces_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'reactivesurfaces-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'reactivesurfaces_resource_hints', 10, 2 );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function reactivesurfaces_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'reactivesurfaces_content_width', 640 );
}
add_action( 'after_setup_theme', 'reactivesurfaces_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function reactivesurfaces_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'reactivesurfaces' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'reactivesurfaces' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'reactivesurfaces_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function reactivesurfaces_scripts() {

	// CSS/Styles

	// Enqueue Google Fonts
	//https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,600,700
	wp_enqueue_style( 'reactivesurfaces-fonts', reactivesurfaces_fonts_url() );

	wp_enqueue_style( 'reactivesurfaces-style', get_stylesheet_uri(), array(), rand(111,9999), 'all' );

	wp_enqueue_style( 'lity-style', '//cdn.jsdelivr.net/npm/lity@2.3.1/dist/lity.min.css' );

	wp_enqueue_style( 'owl-carousel-style', '//cdn.jsdelivr.net/npm/owl.carousel@2.3.4/dist/assets/owl.carousel.min.css' );

	wp_enqueue_style( 'owl-carousel-theme-style', '//cdn.jsdelivr.net/npm/owl.carousel@2.3.4/dist/assets/owl.theme.default.min.css' );

	wp_enqueue_style( 'featherlight-style', '//cdn.jsdelivr.net/npm/featherlight@1.7.13/release/featherlight.min.css' );


	// Javascript

	wp_enqueue_script( 'lity', '//cdn.jsdelivr.net/npm/lity@2.3.1/dist/lity.min.js', array( 'jquery' ), null, true );

	wp_enqueue_script( 'jquery-flip', '//cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js', array( 'jquery' ), null, true );

	wp_enqueue_script( 'ownl-carousel', '//cdn.jsdelivr.net/npm/owl.carousel@2.3.4/dist/owl.carousel.min.js', array( 'jquery' ), null, true );

	wp_enqueue_script( 'featherlight', '//cdn.jsdelivr.net/npm/featherlight@1.7.13/release/featherlight.min.js', array( 'jquery' ), null, true );

	wp_enqueue_script( 'reactivesurfaces-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ), '20151215', true );

	wp_enqueue_script( 'reactivesurfaces-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'reactivesurfaces-scripts', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'reactivesurfaces_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

