<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Reactive_Surfaces
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<header class="page-header">
				<div class="breadcrumb-container">
					<span><a href="/faq">Reactive Surfaces FAQ</a> > Search Results</span>
				</div>

				<div class="search-form-container">
					<?php get_search_form(); ?>
				</div>
			</header><!-- .page-header -->

			<h1 class="page-title"><?php printf( __( 'Search Results', 'reactivesurfaces' ) ); ?></h1>

			<?php if ( have_posts() ) :

				global $wp_query;
				?>

				<div class="results"><?php printf( esc_html__( '%d result(s) for "%s"', 'reactivesurfaces' ), $wp_query->post_count, '<span>' . get_search_query() . '</span>' ); ?></div>

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'search' );

				endwhile;

				the_posts_navigation();

			else : ?>

				<div class="results"><?php printf( esc_html__( 'No results for "%s"', 'reactivesurfaces' ), '<span>' . get_search_query() . '</span>' ); ?></div>

			<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();