( function( $ ) {

    "use strict";

	$( document ).ready( function() {

		// Fix FontAwesome v5.x compatility with Menu Icons plugin
		$( 'i.fa-instagram' ).removeClass( 'fa' ).addClass( 'fab' );
		$( 'i.fa-facebook-square' ).removeClass( 'fa' ).addClass( 'fab' );
		$( 'i.fa-twitter' ).removeClass( 'fa' ).addClass( 'fab' );
		$( 'i.fa-linkedin-square' ).removeClass( 'fa' ).addClass( 'fab' );


		// Content Style 8: Articles Carousel
		if ( $( '.content-style-8' ).length ) {

			var loop, items;

			if ( $( '.content-style-8 .owl-carousel .article' ).length <= 3 ) {
				loop  = false;
				items = $( '.content-style-8 .owl-carousel .article' ).length;
			} else {
				loop  = true;
				items = 3;
			}

			// Initiate Owl Carousel plugin: http://owlcarousel2.github.io/OwlCarousel2/
			$( '.content-style-8 .owl-carousel' ).owlCarousel(
				{
					items: items,
					loop: loop,
					margin: 10,
					nav: true,
					dots: false,
					slideBy: 'page',
					//stagePadding: 30,
					navText: ['<i class="fa fa-arrow-left"></i>','<i class="fa fa-arrow-right"></i>'],
					responsive: {
						0: {
							items: 1
						},
						620: {
							items: 2
						},
						1140: {
							items: items
						}
					},
				}
			);
		}


		// Content Style 12: Items Carousel
		if ( $( '.content-style-12' ).length ) {

			// Initiate Owl Carousel plugin: http://owlcarousel2.github.io/OwlCarousel2/
			$( '.content-style-12 .owl-carousel' ).owlCarousel(
				{
					items: 1,
					loop: true,
					margin: 10,
					nav: true,
					dots: false,
					slideBy: 'page',
					//stagePadding: 30,
					navText: ['<i class="fa fa-arrow-left"></i>','<i class="fa fa-arrow-right"></i>'],
				}
			);
		}


		// Tabs
		$( 'ul.tabs li' ).on( 'click', function() {

			var tabID = $( this ).attr( 'data-tab' );

			$( 'ul.tabs li, .tab-content' ).removeClass( 'active' );
			$( '.tab-content' ).removeClass( 'active' );

			$( this ).addClass( 'active' );
			$( '#' + tabID ).addClass( 'active' );
		} );


		// Content FAQ
		if ( $( '.content-faq' ).length ) {

			// Handle Accordion Open/Close
			$( '.accordion-title' ).on( 'click', function() {

				if ( $( this ).hasClass( 'active' ) ) {
					$( this ).removeClass( 'active' );
					$( this ).find( 'i' ).removeClass( 'fa-minus' ).addClass( 'fa-plus' );
				} else {
					$( this ).addClass( 'active' );
					$( this ).find( 'i' ).removeClass( 'fa-plus' ).addClass( 'fa-minus' );
				}

				$( this ).next( '.panel' ).slideToggle();
			} );
		}

	} );


	$( window ).on( 'load', function() {

		// Content Style 5: Flip Boxes
		if ( $( '.content-style-5' ).length ) {

			var $flipElement = $( '.content-style-5 .column-flip' );

			// Auto adjust flip element height (try this if forceWidth and forceHeight become deprecated)
			$flipElement.height( $( '.content-style-5 .front' ).outerHeight() );

			// Initiate flip plugin: https://nnattawat.github.io/flip/
			$flipElement.flip(
				{
					speed: 320,
					forceWidth: true,
					forceHeight: true,
				}
			);
		}

	} );

} ) ( jQuery );