jQuery( document ).ready( function( $ ) {

	$( '#mobile-navigation-toggle' ).on( 'click', function() {
		$( '#mobile-navigation' ).addClass( 'nav-expanded' );
	} );

	$( '#mobile-navigation-close' ).on( 'click', function() {
		$( '#mobile-navigation' ).removeClass( 'nav-expanded' );
	} );

	$( '#mobile-navigation' ).on( 'scroll touchmove mousewheel', function( e ) {
		e.preventDefault();
		e.stopPropagation();
		return false;
	} );

} );