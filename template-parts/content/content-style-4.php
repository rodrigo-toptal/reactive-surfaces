<section id="" class="content-style-4">

	<div class="video-container">
		<figure class="video-image">
			<div class="transparent-overlay"></div>
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-screenshot.jpg'; ?>">
		</figure>

		<a data-lity href="https://www.youtube.com/watch?v=ls0ayO5FIro" class="mobile-video-player-button">
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-player-button.png'; ?>">
		</a>

		<a data-lity href="https://www.youtube.com/watch?v=ls0ayO5FIro">
			<div class="video-text-container">
				<p>Watch OPDtox at work, protecting lives from chemical weapons</p>
				<p>4:15</p>
			</div>
		</a>
	</div>

</section><!-- .content-style-4 -->