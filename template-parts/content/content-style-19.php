<section id="" class="content-style-19">

	<div class="box image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-19-image-1.jpg">
	</div>

	<div class="box text-container">
		<h2>Vision</h2>
		<div class="text-regular">
			<p>Reactive Surfaces strives to be the best and most innovative bio-engineering company serving the paints and coatings markets worldwide with an environmentally green enzyme and peptide technology.</p>
		</div>
		<div class="text-small">
			<p>Our ultimate goal is to develop products which will provide a healthier and better life for millions.  Reactive Surfaces will continue to innovate, develop and actively promote enzymes and peptides for adding functionality to coatings.</p>
		</div>
	</div>

	<div class="box text-container">
		<h2>Mission</h2>
		<div class="text-regular">
			<p>Our mission is to be the preferred source of innovative, top-quality bioengineered solutions that meet the most stringent environmental conditions, while also addressing the needs of the paints and coatings markets.</p>
		</div>
		<div class="text-small">
			<p>Through innovation and inspiration, Reactive Surfaces enables their customers to build dynamically functional coatings to improve the quality of life.</p>
		</div>
	</div>

	<div class="box image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-19-image-2.jpg">
	</div>

</section><!-- .content-style-19 -->