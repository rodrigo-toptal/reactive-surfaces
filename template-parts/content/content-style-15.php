<section id="" class="content-style-15">

	<div class="text-container">
		<h2>The market for ProteCoat is deep and wide.</h2>
		<p>Wherever coatings go to protect surfaces, ProteCoat can protect against micro-organisms.</p>
		<h3>By 2020:</h3>
	</div>

	<div class="boxes-container">
		<div class="box style-1">
			<a href="#"><p>The market for smart anti-microbials will grow to $1.3 billion.</p></a>
		</div>

		<div class="box style-2">
			<a href="#"><p>Smart multi-functional coatings and surfaces are expected to generate around $120 million.</p></a>
		</div>

		<div class="box style-3">
			<a href="#"><p>Smart antimicrobial coatings and surfaces in non-healthcare markets will reach $225 million.</p></a>
		</div>
	</div>

</section><!-- .content-style-15 -->