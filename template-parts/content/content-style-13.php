<section id="" class="content-style-13">

	<div class="text-container">
		<h2>Let’s compare ours to theirs</h2>
		<p>Microban, whose active ingredient is the preservative tricolsan, has been banned for certain uses by the US FDA. Paint Shield, whose active ingredient is a quaternary silane, has recently been banned by certain US hospital systems and in Europe.</p>
	</div>

	<div class="columns-container">
		<div class="column">
			<h3 class="column-title">Title</h3>
			<div class="column-content">
				<p>Effective against bacteria?</p>
				<p>Fungi? Certain Viruses?</p>
				<p>Toxic</p>
				<p>Ecotoxic</p>
				<p>Develops antibiotic resistance?</p>
			</div>
		</div>

		<div class="column featured">
			<h3 class="column-title">ProteCoat</h3>
			<div class="column-content">
				<p>YES: sporulating &amp; non-sporulating</p>
				<p>YES</p>
				<p>NO</p>
				<p>NO</p>
				<p>YES</p>
			</div>
		</div>

		<div class="column">
			<h3 class="column-title">MicroBan</h3>
			<div class="column-content">
				<p>Only non-sporulating</p>
				<p>YES NO</p>
				<p>Deemed toxic in EU, being studied in US</p>
				<p>Deemed toxic in EU, being studied in US</p>
				<p>Undetermined</p>
			</div>
		</div>

		<div class="column">
			<h3 class="column-title">Paint Shield</h3>
			<div class="column-content">
				<p>YES</p>
				<p>YES NO</p>
				<p>YES: banned in some US hospitals*</p>
				<p>YES: banned in some US hospitals*</p>
				<p>NO</p>
			</div>
		</div>
	</div>

</section><!-- .content-style-13 -->