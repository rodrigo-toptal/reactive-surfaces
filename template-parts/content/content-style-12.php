<section id="" class="content-style-12">

	<div class="text-container">
		<h2>ProteCoat in Our Industries</h2>
		<p></p>
	</div>

	<div class="content-container owl-carousel owl-theme">
		<div class="item">
			<div class="item-text-container">
				<h3>Paint Solutions</h3>
				<h4>Peptides that Protect Against Micro-organism infestation</h4>
				<div class="html-content">
					<ul>
						<li>Developed for antimicrobial use</li>
	  					<li>Developed to be effective against bacteria, fungi & algae</li>
	  					<li>Designed for use in hospitals, industrial, architectural and hygienic coatings, automobiles, textiles, cements</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-image-1.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>Paint Solutions</h3>
				<h4>Peptides that Protect Against Micro-organism infestation</h4>
				<div class="html-content">
					<ul>
						<li>Developed for antimicrobial use</li>
	  					<li>Developed to be effective against bacteria, fungi & algae</li>
	  					<li>Designed for use in hospitals, industrial, architectural and hygienic coatings, automobiles, textiles, cements</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-image-1.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>Paint Solutions</h3>
				<h4>Peptides that Protect Against Micro-organism infestation</h4>
				<div class="html-content">
					<ul>
						<li>Developed for antimicrobial use</li>
	  					<li>Developed to be effective against bacteria, fungi & algae</li>
	  					<li>Designed for use in hospitals, industrial, architectural and hygienic coatings, automobiles, textiles, cements</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-image-1.jpg">
			</figure>
		</div>
	</div>

</section><!-- .content-style-12 -->