<section id="" class="content-style-10">

	<div class="text-container">
		<h2>Protecoat works!</h2>
		<p></p>
	</div>

	<div class="grid-container">
		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-hexagons.png">
			<p>Simple 7 L-Amino Acid Chain</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-shape.png">
			<p>Synergism with existing anti-microbials</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-environment.png">
			<p>No Environmental Persistence</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-metals.png">
			<p>Contains no heavy metals</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-cells.png">
			<p>Vegetative Cells and Spores</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-resistance.png">
			<p>Does NOT cause anti-microbial resistance</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-microbes.png">
			<p>Broad-Range of Susceptible Microbes</p>
		</div>
	</div>

</section><!-- .content-style-10 -->