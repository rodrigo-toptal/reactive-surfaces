<section id="" class="content-style-18">

	<div class="top-container">
		<div class="team-member-container">
			<figure class="image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-steve.jpg">
			</figure>
			<div class="position-container">
				<p>Steve McDaniel</p>
				<p>Operations Lead</p>
			</div>
		</div>

		<div class="team-member-container">
			<figure class="image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-beth.jpg">
			</figure>
			<div class="position-container">
				<p>Beth McDaniel</p>
				<p>Marketing Director</p>
			</div>
		</div>

		<div class="text-container">
			<h2>The Partners</h2>
			<p>Inspired by the diversity and efficacy of naturally-occurring biological molecules, Reactive Surfaces is harnessing the power of molecular engineering and developing bio-based additives.</p>
		</div>
	</div>

	<div class="members-container">
		<div class="team-member-container">
			<figure class="image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-1.jpg">
			</figure>
			<div class="position-container">
				<p>Michael Vick</p>
				<p>Operations Director</p>
			</div>
		</div>

		<div class="team-member-container">
			<figure class="image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-2.jpg">
			</figure>
			<div class="position-container">
				<p>Michelle Albany</p>
				<p>Marketing Director</p>
			</div>
		</div>

		<div class="team-member-container">
			<figure class="image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-3.jpg">
			</figure>
			<div class="position-container">
				<p>Allen Korshun</p>
				<p>Operations Director</p>
			</div>
		</div>
	</div>

</section><!-- .content-style-18 -->