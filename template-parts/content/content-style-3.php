<section id="" class="content-style-3">

	<div class="text-container">
		<h2>Our Technology</h2>
		<p>We design technologies to meet our clients' needs. Although nature provides endless resources for use in our products, our primary patented and proprietary technology used in paints, coatings and other materials for a multitude of applications in various industries are:</p>
	</div>

	<div class="columns-container">
		<div class="column">
			<a href="#">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-3-image-1.jpg">
				<h3>ProteCoat®</h3>
			</a>
			<p>A line of broad-spectrum, long-lasting, highly effective, non-toxic, and non-polluting biocides*.</p>
		</div>

		<div class="column">
			<a href="#">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-3-image-2.jpg">
				<h3>DeGreez™</h3>
			</a>
			<p>Enzymes that, when incorporated into surfaces and coatings, create self-degreasing surfaces.</p>
		</div>

		<div class="column">
			<a href="#">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-3-image-3.jpg">
				<h3>OPDTox™</h3>
			</a>
			<p>A green pesticide and nerve agent neutralizer.</p>
		</div>
	</div>

</section><!-- .content-style-3 -->