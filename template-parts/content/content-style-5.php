<section id="" class="content-style-5">

	<div class="text-container">
		<h2>The future of paint & coatings</h2>
		<p>Where will other functional coatings be seen in the very near future? Anywhere that nature already has a solution. Coatings that self-repair, anti-fouling coatings that keep algae and barnacles from forming on boat bottoms, coatings that deodorize, coatings that can be recharged and reprogrammed with novel functionality.</p>
	</div>

	<div class="columns-container">
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-image-1.jpg">
				<div class="column-text-container">
					<i class="fa fa-warning"></i>
					<h3>Problem</h3>
					<p>Fouling buildup on ships and boats causes fuel inefficiency in the water.</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>Solution:</h3>
					<p>In-water testing of Reactive Surfaces non-toxic, anti-fouling coatings has shown great promise in third party testing.</p>
				</div>
			</div>
		</div>
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-image-1.jpg">
				<div class="column-text-container">
					<i class="fa fa-warning"></i>
					<h3>Physical forces</h3>
					<p>Composite materials derive their remarkable strength from molecular polymer bonds, which bonds if destroyed by such physical forces cause cataclysmic failure of the structure.</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>Solution:</h3>
					<p>In-water testing of Reactive Surfaces non-toxic, anti-fouling coatings has shown great promise in third party testing.</p>
				</div>
			</div>
		</div>
	</div>

</section><!-- content-style-5 -->