<section id="" class="content-style-6">

	<div class="text-container text-align-center">
		<h2>So, what’s YOUR problem?</h2>
		<p>Chances are that Reactive Surfaces can find a safe and effective solution straight from nature to make YOUR world a better place.</p>
	</div>

</section><!-- .content-style-6 -->