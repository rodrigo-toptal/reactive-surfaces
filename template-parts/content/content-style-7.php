<section id="" class="content-style-7">

	<div class="text-container">
		<h2>Functional Coatings Industries</h2>
		<p>We have created a platform for developing functional coatings in a variety of industries that will revolutionize the paint and coatings industry.</p>
	</div>

	<div class="grid-container">
		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-7-surfaces.png">
			<h3>Surfaces &amp; Coatings</h3>
			<p>Nearly every surface of every man-made item is coated at some point in its manufacturing process.</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-7-kitchen.png">
			<h3>Restaurant &amp; Kitchen</h3>
			<p>Self-Cleaning: Kitchen counters and equipment, flooring</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-7-gps.png">
			<h3>Electronics &amp; Auto</h3>
			<p>Self-Cleaning: Computers, phones and other touchscreen devices, Interior and exterior auto coating, self-driving vehicles</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-7-medical.png">
			<h3>Surfaces &amp; Coatings</h3>
			<p>Nearly every surface of every man-made item is coated at some point in its manufacturing process.</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-7-military.png">
			<h3>Restaurant &amp; Kitchen</h3>
			<p>Self-Cleaning: Kitchen counters and equipment, flooring</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-7-marine.png">
			<h3>Electronics &amp; Auto</h3>
			<p>Self-Cleaning: Computers, phones and other touchscreen devices, Interior and exterior auto coating, self-driving vehicles</p>
		</div>
	</div>

</section><!-- .content-style-7 -->