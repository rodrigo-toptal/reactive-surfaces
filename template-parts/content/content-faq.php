<section id="" class="content-faq">

	<div class="search-form-container">
		<figure class="hero-image">
			<img alt="" src="<?php echo get_field( 'hero_image' ); ?>">
		</figure>

		<?php get_search_form(); ?>
	</div>

	<div class="text-container">
		<h2><?php echo get_field( 'heading' ); ?></h2>
		<?php if ( ! empty( get_field( 'text' ) ) ) : ?>
			<?php echo get_field( 'text' ); ?>
		<?php endif; ?>
	</div>

	<?php  if( have_rows( 'questions_and_answers' ) ) : ?>
		<div class="accordion-container">
			<?php while ( have_rows( 'questions_and_answers' ) ) : the_row(); ?>

				<h3 class="accordion-title"><?php echo get_sub_field( 'question' ); ?><i class="fa fa-plus"></i></h3>
				<div class="panel"><?php echo get_sub_field( 'answer' ); ?></div>

			<?php endwhile; ?>
		</div>
	<?php endif; ?>

</section><!-- .content-faq -->