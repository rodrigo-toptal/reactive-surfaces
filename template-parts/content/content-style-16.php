<section id="" class="content-style-16">

	<div class="text-container">
		<h2>Watch just a few of our technologies at work.</h2>
		<p>Wherever coatings go to protect surfaces, ProteCoat can protect against micro-organisms.</p>
	</div>

	<div class="video-boxes-container">
		<div class="video-box">
			<div class="video-image-container">
				<div class="transparent-overlay"></div>
				<figure class="video-image">
					<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-16-video-screenshot-1.jpg'; ?>">
				</figure>

				<a data-lity href="https://www.youtube.com/watch?v=ls0ayO5FIro" class="video-player-button">
					<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-16-video-player-button.png'; ?>">
				</a>
			</div>
			<div class="video-description">
				<p>Wherever coatings go to protect surfaces, ProteCoat can protect against micro-organisms.</p>
			</div>
		</div>

		<div class="video-box">
			<div class="video-image-container">
				<div class="transparent-overlay"></div>
				<figure class="video-image">
					<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-16-video-screenshot-2.jpg'; ?>">
				</figure>

				<a data-lity href="https://www.youtube.com/watch?v=ls0ayO5FIro" class="video-player-button">
					<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-16-video-player-button.png'; ?>">
				</a>
			</div>
			<div class="video-description">
				<p>Degreez continuously hydrolyzes vegetable based oils upon contact.</p>
			</div>
		</div>

		<div class="video-box">
			<div class="video-image-container">
				<div class="transparent-overlay"></div>
				<figure class="video-image">
					<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-16-video-screenshot-3.jpg'; ?>">
				</figure>

				<a data-lity href="https://www.youtube.com/watch?v=ls0ayO5FIro" class="video-player-button">
					<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-16-video-player-button.png'; ?>">
				</a>
			</div>
			<div class="video-description">
				<p>OPDtox detoxifies organophosphorous nerve agents on contact.</p>
			</div>
		</div>
	</div>

</section><!-- .content-style-16 -->