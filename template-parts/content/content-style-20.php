<section id="" class="content-style-20">

	<div class="text-container">
		<h2>Read about us in the news!</h2>
	</div>

	<div class="boxes-container">
		<div class="box style-1">
			<h3>TECHNOLOGY</h3>
			<p>Wherever coatings go to protect surfaces, ProteCoat can protect against micro-organisms.</p>
			<a>Read Article<i class="fa fa-chevron-right"></i></a>
		</div>

		<div class="box style-2">
			<h3>SCIENCE</h3>
			<p>Wherever coatings go to protect surfaces, ProteCoat can protect against micro-organisms.</p>
			<a>Read Article<i class="fa fa-chevron-right"></i></a>
		</div>

		<div class="box style-3">
			<h3>TECHNOLOGY</h3>
			<p>Wherever coatings go to protect surfaces, ProteCoat can protect against micro-organisms.</p>
			<a>Read Article<i class="fa fa-chevron-right"></i></a>
		</div>
	</div>

</section><!-- .content-style-20 -->