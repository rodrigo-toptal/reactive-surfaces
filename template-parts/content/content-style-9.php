<section id="" class="content-style-9 layout-1">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-9-image-1.jpg">
	</figure>

	<div class="text-container">
		<h2>ProteCoat – peptides that protect against micro-organism infestation.</h2>
		<p>Such as nanosilver and quaterinary silanes in numerous independent 3rd party testing for effectiveness against pathogens, such as bacteria, molds, fungi, algae and certain viruses.</p>
	</div>

	<div class="color-box-container">
		<h2>Did you know?</h2>
		<h3>Our naturally-occurring additives offer distinct functionality as a differential…and that means bottom-line profits to you.</h3>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
	</div>

</section><!-- .content-style-9 -->