<section id="" class="content-style-14">

	<div class="text-container">
		<h2>Let’s compare ours to theirs</h2>
		<p>Oil is actually removed from the surface as opposed to bubbling up on the hydrophobic surface</p>
	</div>

	<div class="columns-container">
		<div class="column featured">
			<h3 class="column-title">Degreez Oleophagic Coating</h3>
			<div class="column-content">
				<p>Gets better with more use</p>
				<p>Better anti-fingerprint action</p>
				<p>Slick</p>
				<p>Scratch-resistant</p>
				<p>Oil is actually removed from the surface as opposed to bubbling up on the hydrophobic surface</p>
				<p>Works for the lifetime of the coating</p>
				<p>Slick coating, once activated, keeps many other contaminants from sticking to surface</p>
			</div>
		</div>

		<div class="column">
			<h3 class="column-title">Leading Hydrophobic Coating</h3>
			<div class="column-content">
				<p>Gets worse with more use</p>
				<p>Doesn’t measure up</p>
				<p>Slick only when new</p>
				<p>Not scratch-resistant</p>
				<p>Oil remains on the surface</p>
				<p>Steadily decreases in effectiveness</p>
				<p>Does nothing to other contaminants</p>
			</div>
		</div>
	</div>

</section><!-- .content-style-14 -->