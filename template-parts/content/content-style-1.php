<section id="" class="content-style-1">

	<div class="column-1">
		<h2>Did you know?</h2>
		<h3>Greasy fingerprints can be a thing of the past?</h3>
		<p>We have developed enzymatic self-cleaning coatings that effectively and continuously break down natural greases, fats and oils on surfaces.</p>
		<p>Let Reactive formulate a self-cleaning coating for your functional coatings line.</p>
		<a href="#" class="button-link">Watch Degreez at work</a>
		<div class="padding-space"></div>
	</div>

	<div class="column-2">
		<h3>BIO-TECH INNOVATION IN A MATERIAL WORLD</h3>
		<h1>REVOLUTIONIZING THE COATINGS WORLD</h1>
		<p>With innovation and product development at the core of what we do, we merge coatings technology with naturally-occurring, non-toxic bio-molecule based additives to create dynamically-functional coatings and other materials.</p>
		<a href="/technology">About Our Technology<i class="fa fa-chevron-right"></i></a>
	</div>

</section><!-- .content-style-1 -->