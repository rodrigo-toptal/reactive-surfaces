<section class="content-style-11">

	<div class="text-container">
		<h2>Let’s compare ours to theirs</h2>
		<p>Microban, whose active ingredient is the preservative tricolsan, has been banned for certain uses by the US FDA. Paint Shield, whose active ingredient is a quaternary silane, has recently been banned by certain US hospital systems and in Europe.</p>
	</div>

	<a href="/wp-content/themes/reactivesurfaces/images/content-style-11-comparison-table-protecoat.jpg" data-lity data-lity-desc="">
		<figure class="image-container">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-11-comparison-table-protecoat.jpg">
		</figure>
	</a>

</section><!-- .content-style-11 -->