<section class="content-style-11">

	<div class="text-container">
		<h2>In all ways, ProteCoat is equal to Microban and Paint Shield ...except where it's BETTER!</h2>
		<p></p>
	</div>

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-11-better.png">
	</figure>

</section><!-- .content-style-11 -->