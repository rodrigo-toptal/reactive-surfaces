<section id="product-2-slider" class="content-style-12">

	<div class="text-container">
		<h2>ProteCoat at work!</h2>
		<p><i class="fa fa-exchange-alt" style="font-size: 2em;"></i></p>
	</div>

	<div class="content-container owl-carousel owl-theme">
		<div class="item">
			<div class="item-text-container">
				<h3>In-Film Functionality</h3>
				<h4>Peptides that Protect Against Micro-organism infestation</h4>
				<div class="html-content">
					<ul>
						<li>Developed for antimicrobial use</li>
	  					<li>Developed to be effective against bacteria, fungi &amp; algae</li>
	  					<li>Designed for use in hospitals, industrial, architectural and hygienic coatings, automobiles, textiles, cements</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-image-1.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>In-Can Functionality</h3>
				<h4>ProteCoat as an in-can preservative has been proven effective at eliminating broad-spectrum bacteria</h4>
				<div class="html-content">
					<ul>
						<li>At Reactive Surfaces, we tailor the formulation to be effective against the bacterial challenges associated with the intended application.</li>
	  					<li>Eliminating in-can bacteria increases paint longevity on the shelf</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-image-2.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>Functionality against Algae</h3>
				<h4>ProteCoat has been shown to be effective against common algae on exterior surfaces</h4>
				<div class="html-content">
					<ul>
						<li>ProteCoat works in many applications on a variety of subsurfaces, including concrete, (shown here)</li>
	  					<li>3rd-party, independent testing shows that ProteCoat is non-toxic and environmentally-benign*</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-image-3.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>Functionality against Viruses</h3>
				<h4>Up to now, this has been unheard of in the coatings industry!</h4>
				<div class="html-content">
					<ul>
						<li>Reactive Surfaces testing has shown efficacy against certain viruses, as well as broad spectrum sporulating and non-sporulating bacteria.</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-image-4.jpg">
			</figure>
		</div>
	</div>

</section><!-- .content-style-12 -->