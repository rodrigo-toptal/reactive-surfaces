<section class="content-style-6">

	<div class="text-container text-position-center disclaimer">
		<!-- <h2></h2> -->

		<p>*ProteCoat is not registered as a pesticide with the US Environmental Protection Agency (EPA), pursuant to the Federal Insecticide Fungicide and Rodenticide Act, 7 U.S.C. §136 et seq. (1996). Products containing ProteCoat may require EPA or other regulatory approvals. Reactive Surfaces makes no claim herein that ProteCoat can be sold for use as an antimicrobial to kill pests under the Act without first obtaining any and all required regulatory approvals and registrations.</p>
	</div>

</section><!-- .content-style-6 -->