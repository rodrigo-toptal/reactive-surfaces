<section class="content-style-15">

	<div class="text-container">
		<h2>The market for ProteCoat is deep and wide.</h2>
		<p>Wherever coatings go to protect surfaces, ProteCoat can protect against micro-organisms.</p>
		<h3>By 2020:</h3>
	</div>

	<div class="boxes-container">
		<div class="box style-1">
			<i class="fa fa-quote-left"></i>
			<h3>Reactive Surfaces is at the leading edge of this <strong>"green revolution"</strong> in biocide manufacturing.</h3>
			<p>Next-Generation Antimicrobial Additives for Reactive Surface Coatings</p>
			<p>July 2006</p>
		</div>

		<div class="box style-2">
			<i class="fa fa-quote-left"></i>
			<h3>ProteCoat™: Bio-Engineered Peptide Additives That Work in Coatings</h3>
			<p>Markets for Smart Antimicrobial Coatings and Surfaces – 2015 to 2022</p>
			<p>August 2015</p>
		</div>

		<div class="box style-3">
			<i class="fa fa-quote-left"></i>
			<h3 style="font-size: 0.8em;">Today, smart antimicrobials are dominated by silver-based materials,... The silver-based smart antimicrobials market will... be hurt by the appearance of more effective smart antimicrobials such as PEPTIDES (emphasis added) as well as environmental concerns about silver itself.</h3>
			<p>Markets for Smart Antimicrobial Coatings and Surfaces – 2015 to 2022</p>
			<p>August 2015</p>
		</div>
	</div>

</section><!-- .content-style-15 -->