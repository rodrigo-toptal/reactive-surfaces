<section class="content-style-9 layout-1">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-9-protecoat.jpg">
	</figure>

	<div class="text-container">
		<h2>ProteCoat: Anti&#8209;Microbial Peptides that protect against micro&#8209;organism infestation.*</h2>
		<p>Naturally&#8209;occurring peptides, alone and in combination with other bio-based molecules, have proven effective in numerous independent 3rd party testing against bacteria, molds, fungi, algae, and certain viruses.</p>
	</div>

	<div class="color-box-container bg-color-blue">
		<h2>Did you know?</h2>
		<h3><a href="/news/bio-based-anti-microbial-food-packaging/">Peptides have been the subject of increasing interest in food, materials preservation and food packaging.</a></h3>
		<!-- <p></p> -->
	</div>

</section><!-- .content-style-9 -->