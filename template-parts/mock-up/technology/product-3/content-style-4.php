<section id="product-2-video" class="content-style-4">

	<div class="video-container">
		<figure class="video-image">
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-image-degreez.jpg'; ?>">
		</figure>

		<a data-lity href="https://youtu.be/VjxmTVjof7Q" class="mobile-video-player-button">
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-player-button.png'; ?>">
		</a>

		<a data-lity href="https://youtu.be/VjxmTVjof7Q">
			<div class="video-text-container">
				<p>Watch DeGreez at work, breaking down oils from shampoo right before your eyes!</p>
				<p>0:35</p>
			</div>
		</a>
	</div>

</section><!-- .content-style-4 -->