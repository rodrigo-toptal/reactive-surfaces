<section class="content-style-15">

	<div class="text-container">
		<h2>The market for DeGreez is deep and wide.</h2>
		<p>Self-cleaning materials is a multi-billion dollar market, and Reactive Surfaces DeGreez enzymatic technology is so effective, that products containing this technology will be poised to capture the market share in their industry.</p>
		<h3>By 2020:</h3>
	</div>

	<div class="boxes-container">
		<div class="box style-1">
			<i class="fa fa-quote-left"></i>
			<h3>... the market for self-cleaning materials (including hydrophobic, hydrophilic, electrostatic and catalytic materials) will grow to around $3.3 Billion by 2020.</h3>
			<p>N-tech Research Report: Market for Self-Cleaning Materials will reach US $3.3 Billion by 2020</p>
			<p>October 2015</p>
		</div>

		<div class="box style-2">
			<i class="fa fa-quote-left"></i>
			<h3>This research believes that self-cleaning materials will become one of the largest segments of the smart materials market in terms of revenue generation.</h3>
			<p>Markets and Markets:Smart Coatings Market…Global forecast to 2022</p>
			<p>2018</p>
		</div>

		<div class="box style-3">
			<i class="fa fa-quote-left"></i>
			<h3>...other types of self-cleaning materials technology that we believe will become increasingly commercially successful. These include those based of electrostatic and catalytic approaches.</h3>
			<p>Markets and Markets:Smart Coatings Market…Global forecast to 2022</p>
			<!-- <p></p> -->
		</div>
	</div>

</section><!-- .content-style-15 -->