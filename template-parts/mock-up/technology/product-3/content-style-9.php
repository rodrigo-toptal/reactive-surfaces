<section class="content-style-9 layout-2">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-9-degreez.jpg">
	</figure>

	<div class="text-container">
		<h2>Witness the power of Enzymatic Self&#8209;Cleaning Coatings made with DeGreez</h2>
		<p>Enzymatic self&#8209;cleaning functionality for everything from electronics to autos, solar panels to eyewear, kitchens to sewer pipes.</p>
	</div>

	<div class="color-box-container bg-color-light-green">
		<h2>Did you know?</h2>
		<h3>That Reactive Surfaces' DeGreez enzymatic additive formulated into YOUR coating will actually BREAK DOWN natural greases, fats and oils...on contact! And it works over and over again for the life of the coating..</h3>
		<p>Other anti&#8209;fingerprint coatings merely cause the oils to bead up on the surface. Once these oils smear on the surface, they no longer work.</p>
	</div>

</section><!-- .content-style-9 -->