<section class="content-style-9 layout-1" style="margin-top: 7em;">

	<div class="color-box-container bg-color-blue">
		<h2>Did you know?</h2>
		<!-- <h3></h3> -->
		<p>Because paint is thin and almost weightless with good adherence, it can be used on vertical surfaces reaching as high as we want to go, capturing as much CO2 as we desire? The sky’s the limit!</p>
	</div>

</section><!-- .content-style-9 -->