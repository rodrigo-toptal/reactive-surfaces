<section class="content-style-10">

	<div class="text-container">
		<h2>Carbon Capture Coatings Work</h2>
		<p></p>
	</div>

	<div class="grid-container">
		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-algae.png">
			<p>Formulated to support algae growth</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-weightless.png">
			<p>Thin and almost weightless, so can go very high vertically</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-sunlight.png">
			<p>Can be engineered to allow sunlight, but protect against UV</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-free-of-gases.png">
			<p>Can allow for the free exchange of gases in the atmosphere</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-cellulose.png">
			<p>When sequestered, the by-product, cellulose is formed</p>
		</div>

		<div class="grid-item">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-10-cellulose-repurposed.png">
			<p>Cellulose can be repurposed into many products, such as building materials, clothing, or even plastic bottles</p>
		</div>
	</div>

</section><!-- .content-style-10 -->