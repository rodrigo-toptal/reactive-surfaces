<section class="content-style-8">

	<div class="text-container">
		<h2>IN THE NEWS</h2>
	</div>

	<div class="articles-container owl-carousel owl-theme">
		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/carbon-capture-coatings-proof-of-concept-results-and-call-to-action/">
				<h3>Carbon Capture Coatings: Proof Of Concept Results And Call To Action</h3>
				<p>Coatings Capture Coatings</p>
				<p>June 2019</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/watch-the-interview-about-carbon-capture-coatings-with-reactive-surfaces-founder-steve-mcdaniel/">
				<h3>Watch the interview about Carbon Capture Coatings with Reactive Surfaces' founder, Steve McDaniel</h3>
				<p>Coatings Capture Coatings</p>
				<p>June 2019</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="javascript:void(0);">
				<h3>Coming Soon! 'Can Paint Save Humanity?'<br>Beth McDaniel speaks at the European Women’s Coatings Conference in 2020.</h3>
				<p>Coatings Capture Coatings</p>
				<p>June 2019</p>
			</a>
		</div>

	</div>

	<div class="link-container">
		<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">View All Articles<i class="fa fa-chevron-right"></i></a>
	</div>

</section><!-- content-style-8 -->