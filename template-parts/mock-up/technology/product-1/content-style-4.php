<section class="content-style-4">

	<div class="video-container">
		<figure class="video-image">
			<div class="transparent-overlay"></div>
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-image-carbon-capture-coatings.jpg'; ?>">
		</figure>

		<a data-lity href="https://www.youtube.com/watch?v=FCIPxVkTAgY" class="mobile-video-player-button">
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-player-button.png'; ?>">
		</a>

		<a data-lity href="https://www.youtube.com/watch?v=FCIPxVkTAgY">
			<div class="video-text-container">
				<p>Bottle Tree Farming.</p>
				<p>2:38</p>
			</div>
		</a>
	</div>

</section><!-- .content-style-4 -->