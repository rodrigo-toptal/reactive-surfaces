<section id="join-our-consortium" class="content-style-6">

	<div class="text-container text-align-center text-position-center">
		<h2>JOIN OUR CONSORTIUM</h2>
		<p>Interested in Becoming a Part of the Consortium to Bring Carbon Capture Coatings to Market? Do you or your company or organization have coatings expertise or are you a raw materials supplier looking to help launch this important technology? Do you have other expertise, such as engineering, marketing or distribution networks you can offer up? We want to hear from you. Please fill out the form below and someone will be in touch with you.</p>
		<?php echo do_shortcode( '[contact-form-7 id="209" title="Join Our Consortium"]' ); ?>
		<p><a href="/ccc-faq" class="button-link">Visit our FAQ page</a></p>
	</div>

</section><!-- .content-style-6 -->