<section class="content-style-9 layout-2">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-9-carbon-capture-coatings.jpg">
	</figure>

	<div class="text-container">
		<h2>Carbon Capture Coatings:<br><br>Can Paints and Coatings Save Humanity?</h2>
		<p>Carbon Capture Coatings have the power to significantly reduce carbon dioxide in the atmosphere, thereby lessening the impact of global warming. Carbon Capture Coatings are bio-engineered such that, when exposed to sunlight, they capture and fix atmospheric carbon dioxide. These coatings support living cells capable of carrying out photosynthesis, the process by which Nature captures and fixes atmospheric CO2.</p>
	</div>

	<div class="color-box-container bg-color-light-green" style="font-size: 0.8em;">
		<h2>Did you know?</h2>
		<h3>If we continue on the track we’re on now, by 2050, we will  see:</h3>
		<ul>
			<li>A rise in sea levels by up to 20 inches;</li>
			<li>Heat "beyond the threshold of human survivability:"</li>
			<li>The collapse of ecosystems collapse including the Amazon rainforest, coral reefs, and the Arctic.</li>
			<li>A fall in global crop production by at least 20 percent.</li>
			<li>And the forced migration of billions of people due to unbearable heat, along with food and water shortages.</li>
		</ul>
		<p>-<em>Existential Climate-Related Security Risk-A Scenario Approach</em>, <b>Breakthrough National Center for Climate Restoration</b>, June 2019 Report.</p>
	</div>

</section><!-- .content-style-9 -->