<section id="product-1-slider" class="content-style-12">

	<div class="text-container">
		<h2>Carbon Capture Coatings at Work</h2>
		<p><i class="fa fa-exchange-alt" style="font-size: 2em;"></i></p>
	</div>

	<div class="content-container owl-carousel owl-theme">

		<div class="item">
			<div class="item-text-container">
				<h3>Watch It Working!</h3>
				<h4>In just minutes, the carbon dioxide is captured by the Carbon Capture Coatings</h4>
				<div class="html-content">
					<ul>
						<li>Carbon dioxide uptake (drop in CO2 ppm with time) for the coated PET containers at various algae loading levels measured on the day the PET containers were made.</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-carbon-capture-coatings-1.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>Bottle Trees</h3>
				<h4>Algae grows happily in our coating system</h4>
				<div class="html-content">
					<ul>
						<li>Coated array where each sleeve of PET containers contains increasing amounts of algae from left to right: 0.5%, 1.0%, 2.5%, and 5.0%. Darkening of the coatings with time is evidence of algae health.</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-carbon-capture-coatings-2.jpg">
			</figure>
		</div>


		<div class="item">
			<div class="item-text-container">
				<h3>Various Algae Are Tested in Coatings</h3>
				<h4>We identify the best growth in our coatings systems</h4>
				<div class="html-content">
					<ul>
						<li>CO2 uptake rates tested over a 2 week period for algae cells alone and 50/50 algae cells:latex resin wet coating.</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-carbon-capture-coatings-3.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>Carbon Capture Coatings Survive the Test of Time</h3>
				<h4>Even after 45 days, the algae still does the job!</h4>
				<div class="html-content">
					<ul>
						<li>Carbon dioxide uptake for the coated PET containers at 1% algae loading level tested on 0 and 45 days after the coatings were made.</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-carbon-capture-coatings-4.jpg">
			</figure>
		</div>

		<div class="item">
			<div class="item-text-container">
				<h3>Carbon Capture Coatings Produce Cellulose!</h3>
				<h4></h4>
				<div class="html-content">
					<ul>
						<li>Cellulose is used to make building materials, plastic replacements, paper, rayon, and much much more.</li>
					</ul>
				</div>
			</div>

			<figure class="item-image-container">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-12-carbon-capture-coatings-5.jpg">
			</figure>
		</div>

	</div>

</section><!-- .content-style-12 -->