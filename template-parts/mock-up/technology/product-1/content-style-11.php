<section class="content-style-11">

	<div class="text-container">
		<h2>LET'S COMPARE OURS TO OTHER NET EMISSION CARBON CAPTURE TECHNOLOGIES</h2>
		<p>Other Net Emission Technologies (NET's) offer partial, potential solutions to climate change, yet each has their limitations. Some require vast amounts of land, which, if implemented, would require large-scale displacement of people or farmland.  Some are so expensive, they may not see the light of day. Carbon Capture Coatings on massive, vertical surfaces could provide a workable solution. Let's compare ours to theirs…</p>
	</div>

	<a href="/wp-content/themes/reactivesurfaces/images/content-style-11-comparison-table-carbon-capture-coatings.jpg" data-lity data-lity-desc="">
		<figure class="image-container">
			<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-11-comparison-table-carbon-capture-coatings.jpg">
		</figure>
	</a>

</section><!-- .content-style-11 -->