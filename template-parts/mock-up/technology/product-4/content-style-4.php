<section id="product-3-video" class="content-style-4">

	<div class="video-container">
		<figure class="video-image">
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-image-opdtox.jpg'; ?>">
		</figure>

		<a data-lity href="https://youtu.be/P7guqgyv_kQ" class="mobile-video-player-button">
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-player-button.png'; ?>">
		</a>

		<a data-lity href="https://youtu.be/P7guqgyv_kQ">
			<div class="video-text-container">
				<p>Watch OPDtox at work, protecting lives from chemical weapons</p>
				<p>1:01</p>
			</div>
		</a>
	</div>

</section><!-- .content-style-4 -->