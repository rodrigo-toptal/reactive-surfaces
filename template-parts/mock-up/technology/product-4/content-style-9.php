<section class="content-style-9 layout-1">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-9-opdtox.jpg">
	</figure>

	<div class="text-container">
		<h2>Enzymatic additives that decontaminate surfaces of Organophosphorous chemical weapons upon contact.</h2>
		<p>WMDtox is capable of being applied to surfaces common to a war fighters' environment, and its decontamination capability has been successfully demonstrated in both in-house and 3rd party testing against many of the most-common and destructive nerve weapons and nerve weapon analogs.</p>
	</div>

	<div class="color-box-container bg-color-blue">
		<!-- <h2>Did you know?</h2> -->
		<h3 style="margin: 0; font-size: 3em; line-height: 1;"><i class="fa fa-quote-left"></i></h3>
		<p>This technology could be of significant importance in dealing with sensitive equipment or vehicle/facility interior decontamination. While it may not eliminate the need for decontamination in these situations, it could reduce the extent of decontamination required as well as the potential concentration of agent vapors.</p>
		<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--NATO Army Armaments Group Decision sheet September 2002</p>
	</div>

</section><!-- .content-style-9 -->