<section class="content-style-3">

	<div class="text-container">
		<h2>Our Technology</h2>
		<p>We design technologies to meet our clients' needs. Although nature provides endless resources for use in our products, our primary patented and proprietary technology used in paints, coatings and other materials for a multitude of applications in various industries are:</p>
	</div>

	<div class="columns-container">
		<div class="column">
			<a href="/technology?active_tab=tab1">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-3-image-1.jpg">
				<h3>Carbon Capture Coatings</h3>
			</a>
			<p>Polymeric materials that can thinly be spread on a surface, adhere to the surface and then curing. Exposed to sunlight, they will photosynthesise, capturing and fixing carbon, using the photosynthetic process to carbohydrate.</p>
		</div>

		<div class="column">
			<a href="/technology?active_tab=tab2">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-3-image-2.jpg">
				<h3>ProteCoat®</h3>
			</a>
			<p>A line of broad&#8209;spectrum, long&#8209;lasting, highly effective, non&#8209;toxic, and non&#8209;polluting biocides*.</p>
		</div>

		<div class="column">
			<a href="/technology?active_tab=tab3">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-3-image-3.jpg">
				<h3>DeGreez™</h3>
			</a>
			<p>Enzymes that, when incorporated into surfaces and coatings, create self&#8209;degreasing surfaces.</p>
		</div>

		<div class="column">
			<a href="/technology?active_tab=tab4">
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-3-image-4.jpg">
				<h3>OPDTox™</h3>
			</a>
			<p>A green pesticide and nerve agent neutralizer.</p>
		</div>
	</div>

</section><!-- content-style-3 -->