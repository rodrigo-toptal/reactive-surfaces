<section class="content-style-2">

	<div class="content-container">
		<a href="/news/proteins-and-peptides-as-replacement-for-traditional-organic-biocides-part-1/">
			<div class="article">
				<i class="fa fa-quote-left"></i>
				<h3>Proteins and Peptides as Replacements for Traditional Organic Preservatives: Part I</h3>
				<p>Coating Worlds</p>
				<p>April 2018</p>
			</div>
		</a>

		<div class="blocks-container">
			<div class="block-1">
				<h2>Inspired by the diversity and efficacy of naturally&#8209;occurring biological molecules, we set your coatings apart from all the rest!</h2>
				<h3>Harnessing the power of nature and molecular engineering, we can bring highly efficacious, diverse, dynamic functionality to YOUR coating.</h3>
			</div>

			<div class="block-2">
				<h2>Did you know?</h2>
				<h3>Coatings can save soldiers and first responders’ lives.</h3>
				<p>Reactive Surfaces’ WMDtox coating decontaminates organophosphorous chemical weapons, also called nerve gases, upon contact!</p>
				<a href="/technology?active_tab=tab4#product-3-video" class="button-link">Watch WMDtox in action</a>
			</div>

			<div class="block-3">
				<h3>REACTIVE SURFACES IS DEVELOPING COATINGS POWERED BY</h3>
				<p>Non&#8209;toxic, environmentally&#8209;benign peptides that provide broad-spectrum protection against, not only bacteria, but also algae, fungi and some viruses.</p>
				<a href="/technology?active_tab=tab2#product-2-slider" class="button-link">See ProteCoat at work</a>
			</div>
		</div>
	</div>

</section><!-- .content-style-2 -->