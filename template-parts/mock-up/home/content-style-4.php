<section class="content-style-4">

	<div class="video-container">
		<figure class="video-image">
			<div class="transparent-overlay"></div>
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-image-degreez.jpg'; ?>">
		</figure>

		<a data-lity href="https://www.youtube.com/watch?v=VjxmTVjof7Q" class="mobile-video-player-button">
			<img alt="" src="<?php echo get_stylesheet_directory_uri() . '/images/content-style-4-video-player-button.png'; ?>">
		</a>

		<a data-lity href="https://www.youtube.com/watch?v=VjxmTVjof7Q">
			<div class="video-text-container">
				<p>Watch as bathroom caulk containing our DeGreez additive breaks down oils on surfaces left from shampoo</p>
				<p>0:35</p>
			</div>
		</a>
	</div>

</section><!-- .content-style-4 -->