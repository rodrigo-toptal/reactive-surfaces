<section id="" class="content-style-5">

	<div class="text-container">
		<h2>The future of paints and coatings is here.</h2>
		<p>Reactive Surfaces can add dynamic functionality to YOUR product! We don’t make paint...we just make paint work for you! Imagine the distinction your product will achieve with non-toxic, bio-based functionality. Click <a href="/contact/">here</a> to contact us!</p>
	</div>

	<div class="columns-container">
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-in-can.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>In-can paint spoilage</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>In-can preservatives designed to target common paint microbes</p>
				</div>
			</div>
		</div>
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-fingerprints.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>Fingerprints on electronics</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>Optically-clear DeGreez self-cleaning coating for electronics.</p>
				</div>
			</div>
		</div>
	</div>

</section><!-- content-style-5 -->