<section class="content-style-8">

	<div class="text-container">
		<h2>Press &amp; Awards</h2>
	</div>

	<div class="articles-container owl-carousel owl-theme">
		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/carbon-capture-coatings-proof-of-concept-results-and-call-to-action/">
				<h3>Carbon Capture Coatings: Proof Of Concept Results And Call To Action</h3>
				<p>Coatings Capture Coatings</p>
				<p>June 2019</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/bio-based-anti-microbial-food-packaging/">
				<h3>Bio-Based Anti-Microbial Food Packaging</h3>
				<p>Coatings Tech</p>
				<p>September 2018</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/proteins-and-peptides-as-replacement-for-traditional-organic-biocides-part-1/">
				<h3>Proteins and Peptides as Replacement for Traditional Organic Biocides; Part 1</h3>
				<p>Coatings Tech</p>
				<p>April 2018</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/anti-fingerprint-coatings-for-touchscreens/">
				<h3>Anti-Fingerprint Coatings for Touchscreens</h3>
				<p>APCJ</p>
				<p>August 2014</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/wmdtox-coatings-self-decontamination-becomes-a-reality/">
				<h3>WMDtox Coatings: Self-Decontamination Becomes a Reality</h3>
				<p>Coatings World</p>
				<p>July 2013</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/functional-coatings-bring-extended-space-mission-closer/">
				<h3>Functional Coatings Bring Extended Space Mission Closer</h3>
				<p>APCJ</p>
				<p>September 2012</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/chemical-resistence-of-bio-additives-in-epoxy-tank-coatings/">
				<h3>Chemical Resistence of Bio-Additives in Epoxy Tank Coatings</h3>
				<p>PPCJ</p>
				<p>July 2012</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/expanding-opportunities-for-functional-surfaces-self-degreasing-laminates/">
				<h3>Expanding Opportunities for Functional Surfaces: Self-Degreasing Laminates</h3>
				<p>Coatings Tech</p>
				<p>January 2012</p>
			</a>
		</div>

		<div class="article">
			<i class="fa fa-quote-left"></i>
			<a href="/news/stretching-paint-performance/">
				<h3>Stretching Paint Performance</h3>
				<p>Chemical and Engineering News</p>
				<p>June 2011</p>
			</a>
		</div>

	</div>

	<div class="link-container">
		<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">View All Articles<i class="fa fa-chevron-right"></i></a>
	</div>

</section><!-- content-style-8 -->