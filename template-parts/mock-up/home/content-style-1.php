<section class="content-style-1">

	<div class="column-1">
		<h2>Did you know?</h2>
		<h3>That coatings could play a major role in offsetting the catastrophic impact of climate change?</h3>
		<p>Find out more about our Carbon Capture Coatings Technology by <a href="/technology?active_tab=tab1" style="color: white; font-weight: 600;">clicking here</a>. We can’t do this alone...We need industry partners in our mission to fight global warming. Learn about how you can be a part of our coatings technology consortium by clicking below:</p>
		<!-- <p>Let Reactive formulate a self-cleaning coating for your functional coatings line.</p> -->
		<a href="/ccc-faq" class="button-link">Carbon Capture Coatings FAQ</a>
		<div class="padding-space"></div>
	</div>

	<div class="column-2">
		<h3>BIO-TECH INNOVATION IN A MATERIAL WORLD</h3>
		<h1>REVOLUTIONIZING THE COATINGS WORLD</h1>
		<p>With innovation and product development at the core of what we do, we merge coatings technology with naturally&#8209;occurring, non&#8209;toxic bio&#8209;molecule based additives to create dynamically-functional coatings and other materials.</p>
		<a href="/technology">About Our Technology<i class="fa fa-chevron-right"></i></a>
	</div>

</section><!-- .content-style-1 -->