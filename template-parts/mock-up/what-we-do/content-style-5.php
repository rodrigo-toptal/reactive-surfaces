<section id="" class="content-style-5">

	<div class="text-container text-position-center">
		<h2>Reactive Surfaces wants to know, what’s YOUR problem?!</h2>
		<p>Where will other functional coatings be seen in the very near future? Anywhere that nature already has a solution. Anti-fouling coatings that keep algae and barnacles from forming on boat bottoms, self-healing and deodorizing coatings, rechargeable coatings with novel functionality.</p>
	</div>

	<div class="columns-container">
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-ship.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>Fouling buildup on ships and boats causes fuel inefficiency in the water.</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>Initial in&#8209;water testing showed very promising anti&#8209;fouling results, eliminating certain bacteria from ship hulls.</p>
				</div>
			</div>
		</div>
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-food.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>Food spoilage</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>Coatings applied to food packaging that help preserve food.</p>
				</div>
			</div>
		</div>
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-sewage.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>Grease build-up in sewer pipes</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>DeGreez-powered self-cleaning coating.</p>
				</div>
			</div>
		</div>
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-antibiotics.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>Transfer of Anti-biotic Resistance in Biofilms</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>Coatings capable of eliminating nucleic acid polymers like DNA and RNA that carry resistance genes</p>
				</div>
			</div>
		</div>
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-shiny-auto.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>Shiny auto coatings show too many fingerprints</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>DeGreez self-cleaning coating continuously breaks down natural greases greases, fats and oils, leaving the surface cleaner and breaking down fingerprints.</p>
				</div>
			</div>
		</div>
		<div class="column-flip">
			<div class="front">
				<div class="transparent-overlay"></div>
				<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-5-hospital.jpg">
				<div class="column-text-container">
					<i class="fas fa-brush"></i>
					<h3>The Problem:</h3>
					<p>Hospital-acquired infections</p>
				</div>
			</div>
			<div class="back">
				<div class="column-text-container">
					<h3>The Solution:</h3>
					<p>Reactive Surfaces has developed coatings that exhibit efficacy against bacteria, mold, fungi, algae, and certain viruses.* (*Not registered for use as an anti-microbial by the US EPA)</p>
				</div>
			</div>
		</div>
	</div>

</section><!-- .content-style-5 -->