<section class="content-style-9 layout-1">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-9-what-we-do.jpg">
	</figure>

	<div class="text-container">
		<h2>Innovating solutions to real‑world problems.</h2>
		<p>Reactive Surfaces uses its unique, proprietary technologies to solve real&#8209;world problems,...from day to day issues, such as grease build-up on kitchen counters to anti-microbial coatings for food packaging to military coatings that detoxify nerve gases. Seizing on the power of naturally-occurring, non-toxic biological molecules, we develop dynamically-functional paints coatings and other materials that actually react to their environment.</p>
	</div>

	<div class="color-box-container bg-color-dark-blue">
		<h2>From proof-of-concept and prototype development all the way to the commercialized product</h2>
		<h3>Reactive Surfaces works with your team to add functionality, value and distinction to your product line.</h3>
		<!-- <p></p> -->
	</div>

</section><!-- .content-style-9 -->