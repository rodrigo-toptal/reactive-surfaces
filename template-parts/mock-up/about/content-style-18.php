<section class="content-style-18">

	<div class="members-container top-container">
		<div class="team-member-container">
			<a href="#" data-featherlight="#top-member-1">
				<figure class="image-container">
					<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-steve.jpg">
				</figure>

				<div class="position-container">
					<p>Steve McDaniel</p>
					<p>CEO and Chief Innovation Officer</p>
				</div>
			</a>
			<div id="top-member-1" class="bio-popup">
				<h2>Dr. C. Steven McDaniel</h2>
				<h3>Managing partner, Chief Executive Officer and Chief Innovation Officer</h3>
				<p>Dr. McDaniel is the founder and Chief Innovation Officer of Reactive Surfaces since 2001. Dr. McDaniel received a Bachelor of Science in biology from the University of Texas in 1974, a master’s degree in genetics from Texas A&M University in 1976 and a Ph.D. in biochemistry from Texas A&M in 1985. He was a post-doctoral fellow at Baylor College of Medicine from 1985 to 1987, specializing in synthetic peptide chemistry. He obtained his law degree from the University of Houston in 1991. Afterward, Dr. McDaniel specialized in patent law and intellectual property litigation. Mr. McDaniel is a featured speaker on topics ranging from functional coatings to astrobiology and the use of functional coatings in space. Mr. McDaniel and his wife, Beth McDaniel are law partners in the firm, McDaniel and Associates, PC (www.techologylitigators.com), which serves as the law firm for Reactive Surfaces.</p>
			</div>
		</div>

		<div class="team-member-container">
			<a href="#" data-featherlight="#top-member-2">
				<figure class="image-container">
					<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-beth.jpg">
				</figure>
				<div class="position-container">
					<p>Beth McDaniel</p>
					<p>President</p>
				</div>
			</a>
			<div id="top-member-2" class="bio-popup">
				<h2>Beth McDaniel</h2>
				<h3>JD, Partner, President</h3>
				<p>Beth McDaniel received her BBA in finance from the University of Texas in 1988, and JD in law from South Texas College of Law in 1996. She has been with Reactive Surfaces since 2006, both as a director/advisor and officer of the company. Beth has 25 years of business experience and 17 years of legal experience with an emphasis contracts, licensing, and business operations. Ms. McDaniel serves as President and Chief of Administration for Reactive Surfaces, assisting in implementing ongoing company business, business transactions, and office management.</p>
			</div>
		</div>

		<div class="team-member-container">
			<a href="#" data-featherlight="#top-member-3">
				<figure class="image-container">
					<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-jonathan.jpg">
				</figure>
				<div class="position-container">
					<p>Jonathan Hurt</p>
					<p>Vice President</p>
				</div>
			</a>
			<div id="top-member-3" class="bio-popup">
				<h2>Dr. Jonathan Hurt</h2>
				<h3>Vice President</h3>
				<p>Dr. Hurt is a registered United States patent agent specializing in the preparation of patent applications for the biotechnology and polymer chemistry industries, focusing on coatings, elastomers, plastics, composites, proteins, peptides, and molecular biological inventions. Jonathan concentrates his practice in the growing field of integrating material sciences with biotechnology.  He received his B.S. in Biology in 1989 from Stetson College, and his PhD in Biochemistry and Molecular Biology in 1996 from the University of Florida.</p>
			</div>
		</div>
	</div>

	<div class="text-container">
		<h2>The Reactive Surfaces Team</h2>
		<p>Each of our team members makes a unique and important contribution of experience and expertise to drive innovation, product development and commercial success.</p>
	</div>

	<div class="members-container">
		<div class="team-member-container">
			<a href="#" data-featherlight="#team-member-1">
				<figure class="image-container">
					<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-2.jpg">
				</figure>
				<div class="position-container">
					<p>Lisa Kemp</p>
					<p>Chief Scientific Officer</p>
				</div>
			</a>
			<div id="team-member-1" class="bio-popup">
				<h2>Dr. Lisa Kemp</h2>
				<h3>Chief Scientific Officer</h3>
				<p>Lisa joined Reactive Surfaces in 2016 as the Chief Scientific Officer and manages the research laboratories located within the University of Southern Mississippi’s Technology Accelerator.  Lisa received her PhD in Polymer Science and Engineering with a Technology Commercialization focus in 2007.  Prior to receiving her PhD, she worked as an R&D where she developed and commercialized several coatings technologies.  Lisa oversees all commercial product development for RSL and manages the RSL lab team.  Lisa is a member of the Women Chemists Committee of the American Chemical Society, and she is an inaugural member of the Fortune 500 W.O.M.E.N in America group, which aims to advance promising professional women. Lisa enjoys volunteering at local science fairs and working with students who are interested in STEM careers.</p>
			</div>
		</div>

		<div class="team-member-container">
			<a href="#" data-featherlight="#team-member-2">
				<figure class="image-container">
					<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-1.jpg">
				</figure>
				<div class="position-container">
					<p>Tyler Hodges</p>
					<p>Chief of Microbiology</p>
				</div>
			</a>
			<div id="team-member-2" class="bio-popup">
				<h2>Dr. Tyler Hodges</h2>
				<h3>Chief of Microbiology</h3>
				<p>Tyler joined the Reactive Surfaces team in 2014. Tyler holds B.S. and M.S. degrees in Pharmaceutical Sciences with an emphasis in natural products drug discovery and a Ph.D. in Microbiology.  He has been involved in biomolecule discovery and development for the last 15 years from a variety of sources including plants, marine invertebrates, and microorganisms.  He has isolated microbes from unique and extreme habitats, including deep-sea vent environments as well as marine cave systems, and analyzed their biomolecule production and molecular biosynthetic pathways.  Tyler develops molecular and microbial assays and oversees microbial aspects of antimicrobial coating and in-can preservative development.</p>
			</div>
		</div>

		<div class="team-member-container">
			<a href="#" data-featherlight="#team-member-3">
				<figure class="image-container">
					<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-3.jpg">
					<!-- <img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-18-member-default.jpg"> -->
				</figure>
				<div class="position-container">
					<p>Brittney McInnis</p>
					<p>Biologist, Computational Scientist</p>
				</div>
			</a>
			<div id="team-member-3" class="bio-popup">
				<h2>Dr. Brittney McInnis</h2>
				<h3>Biologist and Computational Scientist</h3>
				<p>Dr. Brittney McInnis received here B.S. in Biology from the University of Southern Mississippi in 2006, and a B.S. in Computer Science from Oregon State University in 2017. She received her Ph.D. in Cell and Molecular Biology from the University of Alabama in 2012. Brittney joined the Reactive Surfaces Team in 2017, and is currently involved with formulation and testing of bio-based, antimicrobial coatings. She is also interested in applying modern computational methods for bio-based coating discovery and formulation.</p>
			</div>
		</div>
	</div>

</section><!-- .content-style-18 -->