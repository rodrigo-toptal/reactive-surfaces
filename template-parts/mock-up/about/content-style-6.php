<section class="content-style-6">

	<div class="text-container text-position-right text-decoration">
		<!--  <h2>So, what’s YOUR problem?</h2> -->
		<p>In 2001, following the 911 attacks on the World Trade Center buildings, our founder, Steve McDaniel contemplated whether an enzyme capable of detoxifying organophosphorous pesticides could actually detoxify organophosphorous nerve weapons. He discovered that once it was entrained and stabilized in a coating system, that indeed the enzyme was functional against nerve weapons and remained functional for periods of time. Thus arose our first product…a bioengineered additive capable of constantly and consistently decontaminating and/or remediating chemical and biological threat agents.</p>
		<p>Today, Reactive Surfaces is developing enzyme- and peptide-based additives for self-cleaning surfaces, biocidal surfaces, mold-inhibiting surfaces, deodorizing surfaces, textiles with reactive coatings, self-healing coatings and catalytic column coatings for liquid and gaseous waste-stream decontamination.</p>
		<p>Known in the industry as an innovation machine using cutting-edge bio-engineering to dynamically functionalize paints and coatings, Reactive Surfaces serves markets worldwide with an environmentally green enzyme and peptide technology. Reactive Surfaces develops these products from the proof-of-concept phase all the way to the shelf for its clients; products that will provide a healthier and better lifestyle for millions of people all around the globe.</p>
	</div>

</section><!-- .content-style-6 -->