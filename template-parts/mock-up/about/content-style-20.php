<section class="content-style-20">

	<div class="text-container">
		<h2>RECOGNIZED AS A LEADER IN THE SMART COATINGS INDUSTRY</h2>
	</div>

	<div class="boxes-container">
		<div class="box style-1">
			<i class="fa fa-quote-left"></i>
			<h3>Top Manufacturer/ Player…Reactive Surfaces...</h3>
			<p>RNR Market Research; United States Smart Polymers Market Report 2018</p>
			<p>March 2018</p>
		</div>

		<div class="box style-1">
			<i class="fa fa-quote-left"></i>
			<h3>Major Player in Smart Coatings Market... Reactive Surfaces...</h3>
			<p>Markets and Markets; Smart Polymers Market...Global Forecast to 2022</p>
			<p>October 2017</p>
		</div>

		<div class="box style-1">
			<i class="fa fa-quote-left"></i>
			<h3>Key Player... Reactive Surfaces...</h3>
			<p>N-Tech; Multifunctional Smart Coatings and Surfaces: 2016-2023</p>
			<p>March 2016</p>
		</div>
	</div>

</section><!-- .content-style-20 -->