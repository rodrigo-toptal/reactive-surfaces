<section class="content-style-17" style="background-image: url('/wp-content/themes/reactivesurfaces/images/content-style-17-background-image.jpg');">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-17-image.jpg">
	</figure>

	<div class="text-container">
		<h2>Award-Winning Technology</h2>
		<h3>Reactive Surfaces – WINNER of the coveted American Coatings Award in 2008</h3>
		<p>“The most innovative coatings technology of 2008 and one that will change the coatings industry for the next 50 years."</p>
		<a class="button-link" href="#">Read More</a>
	</div>

</section><!-- .content-style-17 -->