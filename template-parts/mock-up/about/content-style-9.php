<section class="content-style-9 layout-2">

	<figure class="image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-9-about.jpg">
	</figure>

	<div class="text-container">
		<h2>Harnessing the power of nature, Reactive Surfaces adds dynamic functionality to paints and coatings...<br>"Bringing Coatings To Life™."</h2>
		<p>Employing proprietary and environmentally&#8209;friendly enzyme and peptide technology, Reactive Surfaces strives to be the best and most innovative bioengineering company serving the worldwide paint and coatings markets.</p>
	</div>

	<!--
	<div class="color-box-container">
		<h2>Did you know?</h2>
		<h3>Our naturally-occurring additives offer distinct functionality as a differential…and that means bottom-line profits to you.</h3>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
	</div>
	-->

</section><!-- .content-style-9 -->