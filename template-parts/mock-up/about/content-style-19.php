<section class="content-style-19">

	<div class="box image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-19-image-1.jpg">
	</div>

	<div class="box text-container">
		<h2>Vision</h2>
		<div class="text-regular">
			<p>The Reactive Surfaces vision is to develop technology to enhance peoples lives everywhere with coatings that react to their environment in a positive way.</p>
		</div>
		<div class="text-small">
			<p>Our focus is to provide the coatings industry with safe, environmentally benign alternatives to real‑world issues, such as the spread of disease, food safety, medical device innovation and improvements to solar efficiency and the protection of people from weapons of mass destruction. In every decision we make, we consider the potential impact on the people of the world, the environment and the future of our children and the earth.</p>
		</div>
	</div>

	<div class="box text-container">
		<h2>Bio&#8209;engineered Coatings Solutions</h2>
		<div class="text-regular">
			<p>Our mission is to be the preferred source of innovative, top&#8209;quality bioengineered solutions that meet the most stringent environmental conditions, while also addressing the needs of the paints and coatings markets.</p>
		</div>
		<div class="text-small">
			<p>Through innovation, expertise and inspiration, Reactive Surfaces designs and develops unique and proprietary dynamic-functionality for our clients’ specific products, provide their customers a competitive edge in the paints and coatings industry.</p>
		</div>
	</div>

	<div class="box image-container">
		<img alt="" src="/wp-content/themes/reactivesurfaces/images/content-style-19-image-2.jpg">
	</div>

</section><!-- .content-style-19 -->