<?php
/**
 * Template part for displaying page content in front-page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Reactive_Surfaces
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<?php

		// Temporary mock-up snippet

		if ( is_page( "Home" ) ) {

			$folder = "home";
			$modules = array(
				'style-1',
				'style-2',
				'style-3',
				'style-4',
				'style-5',
				'style-6',
				'style-7',
				'style-8',
			);

		} elseif ( is_page( "Technology" ) ) {

			// Not in use (It uses specific Page Template)
			/*
			$folder = "technology";
			$modules = array(

			);*/

		} elseif ( is_page( "What We Do" ) ) {

			$folder = "what-we-do";
			$modules = array(
				'style-9',
				'style-5',
				'style-16',
			);

		} elseif ( is_page( "About" ) ) {

			$folder = "about";
			$modules = array(
				'style-9',
				'style-6',
				'style-18',
				'style-19',
				'style-17',
				'style-20',
			);

		} elseif ( is_page( "News" ) ) {

			$folder = "news";
			$modules = array(

			);

		} else {

			$modules = array();
		}

		foreach ( $modules as $module ) {
			get_template_part( 'template-parts/mock-up/' . $folder . '/content', $module );
		}

		// End Temporary mock-up snippet


		// TODO Uncomment this when integrating with ACF/Back-end to use correct template files
		/*foreach ( $modules as $module ) {
			get_template_part( 'template-parts/content/content', $module );
		}*/
		?>

	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->