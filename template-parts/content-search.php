<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Reactive_Surfaces
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php //reactivesurfaces_search_results_entry_meta(); ?>

		<?php /*if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php
			reactivesurfaces_posted_on();
			reactivesurfaces_posted_by();
			?>
		</div><!-- .entry-meta -->
		<?php endif; */?>
	</header><!-- .entry-header -->

	<?php //reactivesurfaces_post_thumbnail(); ?>

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php //reactivesurfaces_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
