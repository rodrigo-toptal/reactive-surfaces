<?php
/**
 * Template part for displaying FAQ page content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Reactive_Surfaces
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<?php

		$module = 'faq';
		get_template_part( 'template-parts/content/content', $module );

		?>

	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->