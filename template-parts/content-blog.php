<?php
/**
 * Template part for displaying Blog page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Reactive_Surfaces
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php reactivesurfaces_post_thumbnail(); ?>

	<header class="entry-header">
		<?php //reactivesurfaces_the_category_list(); ?>

		<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>

	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php //the_excerpt(); ?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
		$journals = get_the_terms( get_the_ID(), 'journal' );

		if ( $journals && ! is_wp_error( $journals ) ) {
			?>
			<div class="article-meta">
				<div class="article-journal"><?php echo $journals[0]->name; ?></div>
				<div class="article-date"><?php echo get_the_date( 'F Y' ); ?></div>
			</div>
			<?php
		}
		?>

		<?php
		$read_more_link = sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Read More <i class="fa fa-arrow-right"></i><span class="screen-reader-text"> "%s"</span>', 'reactivesurfaces' ),
				array(
					'span' => array(
						'class' => array(),
					),
					'i' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		);

		?>

		<a class="read-more-link" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php echo $read_more_link; ?></a>

		<?php //reactivesurfaces_entry_footer(); ?>

	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
