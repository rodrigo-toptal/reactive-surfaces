<?php
/**
 * Template part for displaying page content in front-page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Reactive_Surfaces
 */

// Check for Active tabs
if ( isset( $_GET['active_tab'] ) && ! empty( $_GET['active_tab'] ) ) {

	if ( "tab1" == $_GET['active_tab'] ) {
		$tab1_active = 'active';
	} elseif ( "tab2" == $_GET['active_tab'] ) {
		$tab2_active = 'active';
	} elseif ( "tab3" == $_GET['active_tab'] ) {
		$tab3_active = 'active';
	} elseif ( "tab4" == $_GET['active_tab'] ) {
		$tab4_active = 'active';
	}

} else {
	$tab1_active = 'active';
}

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<ul class="tabs">
			<li class="tab-link <?php echo esc_attr( $tab1_active ); ?>" data-tab="tab-1">Carbon Capture Coatings</li>
			<li class="tab-link <?php echo esc_attr( $tab2_active ); ?>" data-tab="tab-2">ProteCoat&reg;</li>
			<li class="tab-link <?php echo esc_attr( $tab3_active ); ?>" data-tab="tab-3">DeGreez&trade;</li>
			<li class="tab-link <?php echo esc_attr( $tab4_active ); ?>" data-tab="tab-4">OPDtox&trade;</li>
		</ul>

		<div id="tab-1" class="tab-content <?php echo esc_attr( $tab1_active ); ?>  no-padding">
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-6-1' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-9-1' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-10' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-12' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-9-2' ); ?>
			<?php //get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-11' ); Comparison Chart (Disabled Temporarily) ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-8' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-4' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-1/content', 'style-6-2' ); ?>

			<?php //get_template_part( 'template-parts/content/content', 'style-6' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-9' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-10' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-12' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-9' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-11' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-8' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-4' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-6' ); ?>
		</div>

		<div id="tab-2" class="tab-content <?php echo esc_attr( $tab2_active ); ?>">
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-9' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-10' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-11-1' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-12' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-11-2' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-11-3' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-15' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-2/content', 'style-6' ); ?>

			<?php //get_template_part( 'template-parts/content/content', 'style-9' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-10' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-11' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-12' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-11' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-11' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-15' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-6' ); ?>
		</div>

		<div id="tab-3" class="tab-content <?php echo esc_attr( $tab3_active ); ?>">
			<?php get_template_part( 'template-parts/mock-up/technology/product-3/content', 'style-9' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-3/content', 'style-4' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-3/content', 'style-11' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-3/content', 'style-15' ); ?>

			<?php //get_template_part( 'template-parts/content/content', 'style-9' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-4' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-11' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-15' ); ?>
		</div>

		<div id="tab-4" class="tab-content <?php echo esc_attr( $tab4_active ); ?>">
			<?php get_template_part( 'template-parts/mock-up/technology/product-4/content', 'style-9' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-4/content', 'style-4' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-4/content', 'style-6-1' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-4/content', 'style-11' ); ?>
			<?php get_template_part( 'template-parts/mock-up/technology/product-4/content', 'style-6-2' ); ?>

			<?php //get_template_part( 'template-parts/content/content', 'style-9' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-4' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-6' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-11' ); ?>
			<?php //get_template_part( 'template-parts/content/content', 'style-6' ); ?>
		</div>

		<?php
		/*
		// Temporary mock-up snippet

		if ( is_page( "Home" ) ) {

			$folder = "home";
			$modules = array(
				'style-1',
				'style-2',
				'style-3',
				'style-4',
				'style-5',
				'style-6',
				'style-7',
				'style-8',
			);

		} elseif ( is_page( "Technology" ) ) {

			$folder = "technology";
			$modules = array(

			);

		} elseif ( is_page( "What We Do" ) ) {

			$folder = "what-we-do";
			$modules = array(

			);

		} elseif ( is_page( "About" ) ) {

			$folder = "about";
			$modules = array(

			);

		} elseif ( is_page( "News" ) ) {

			$folder = "news";
			$modules = array(

			);

		} elseif ( is_page( "FAQ" ) ) {

			$folder = "faq";
			$modules = array(
				'faq',
			);

		} else {

			$modules = array();
		}

		foreach ( $modules as $module ) {
			get_template_part( 'template-parts/mock-up/' . $folder . '/content', $module );
		}

		// End Temporary mock-up snippet


		// TODO Uncomment this when integrating with ACF/Back-end to use correct template files
		/*foreach ( $modules as $module ) {
			get_template_part( 'template-parts/content/content', $module );
		}*/
		?>

	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->