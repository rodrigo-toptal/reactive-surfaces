<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Reactive_Surfaces
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footer-navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'footer',
				'menu_id'        => 'primary-menu-footer',
			) );
			?>
		</div><!-- .footer-navigation -->

		<div class="site-info">
			<p><b>Reactive Surfaces</b></p>
			<p>
				<a href="mailto:<?php echo esc_attr( antispambot( 'info@reactivesurfaces.com' ) ); ?>"><?php echo esc_html( antispambot( 'info@reactivesurfaces.com' ) ); ?></span>
				<a href="#"><?php echo __( 'Privacy Policy', 'reactivesurfaces' ); ?></a>
				<a href="#"><?php echo __( 'Terms of Use', 'reactivesurfaces' ); ?></a>
			</p>
		</div><!-- .site-info -->

		<div class="footer-logo">
			<?php $footer_logo_url = get_stylesheet_directory_uri() . '/images/logo-black.png'; ?>
			<a href="<?php echo site_url(); ?>" rel="home" itemprop="url">
				<img width="367" height="113" src="<?php echo $footer_logo_url; ?>" class="custom-logo" alt="Reactive Surfaces" itemprop="logo" srcset="<?php echo $footer_logo_url; ?> 367w, <?php echo $footer_logo_url; ?> 300w" sizes="(max-width: 367px) 100vw, 367px">
			</a>
		</div><!-- .footer-logo -->

		<div class="social-media-navigation">
			<p class="stay-in-touch-label"><?php echo __( 'Stay in Touch:', 'reactivesurfaces' ); ?></p>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'social',
				'menu_id'        => 'social-media-footer-links',
			) );
			?>
		</div><!-- .social-media-navigation -->

		<div class="site-copyright">
			<p><?php echo __( 'Reactive Surfaces ' . date( 'Y' ) . '. Copyrights Reserved.', 'reactivesurfaces' ); ?></p>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
